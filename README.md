# lab5-oci-poapproval

Deploy the Purchase Order Approval Mobile Application and configure it with your JDE Trial Instance

## Getting started

The objective of this lab is to deploy the standard Purchase Approval Mobile Application on the JDE Trial Instance server and configure it to run with the JDE Trial instance

## target architecture

  ![JDE PO Mobile Architecture](./images/jdetrial-pomobile-architecture.png)

## lab steps

### Task 1. Install Apache Web Server on JDE Trial Instance

1. Connect to the JDE Trial Instance from OCI Cloud Shell using SSH:

```bash
ssh -i <private-key-file> opc@<jdetrialip>
```

2. Install Apache package and open Firewall port

```bash
sudo yum -y install httpd git;
sudo firewall-cmd --permanent --add-port=80/tcp;
sudo firewall-cmd --permanent --add-port=443/tcp;
sudo firewall-cmd --reload;
```

### Task 2. Install the Purchase Order Mobile Application

1. download and unzip the Purchase Order Approval Mobile application:

```bash
git clone https://gitlab.com/oracle35/oci-hands-on/jdeonoci-day/lab5-oci-poapproval.git
cd lab5-oci-poapproval
mkdir tmp; unzip po_approval_mobile_V3.zip -d tmp/    
```

2. use the script **replace.sh** to update configuration files with JDE Trial Instance IP and AIS Port:

```bash
sh bin/replace.sh xxx.xxx.xxx.xxx 7077
```

3. move PO Mobile files in Apache directory and restart Apache Server

```bash
sudo cp -R tmp/* /var/www/html/
sudo chown -R apache:apache /var/www;
sudo systemctl stop httpd;
sudo systemctl start httpd;
```

4. connect to the PO Mobile URL:

  * http://jdetrialip/

>note: if you want to see existing Purchase Order for approval, you would first need to create them in JD Edwards or you can map your JDE userid to the Address Number ****

  ![JDE PO Mobile Architecture](./images/jdetrial-pomobile-screen.png)
