#grep -rlZ POApprovalAISServer-hostName . --exclude-dir=.folder | xargs -0 sed -i '' -e 's/https\:\/\/POApprovalAISServer-hostName\:port/http\:\/\/144\.24\.195\.200\:7070/g'

if [ $# -eq 0 ]
  then
    echo "Usage: replace.sh IP PORT"
    exit 1
fi
if [ $# -eq 1 ]
  then
    echo "Usage: replace.sh IP PORT"
    exit 1
fi

TARGET_IP=$1
TARGET_PORT=$2

echo "########################################"
echo "Replacing with IP : $TARGET_IP"
echo "Replacing with PORT : $TARGET_PORT"
echo "########################################"

IP1=`echo $TARGET_IP| cut -d'.' -f1`
IP2=`echo $TARGET_IP| cut -d'.' -f2`
IP3=`echo $TARGET_IP| cut -d'.' -f3`
IP4=`echo $TARGET_IP| cut -d'.' -f4`

find tmp -type f -exec sed -i -e "s/https:\/\/POApprovalAISServer-hostName:port/https:\/\/$IP1\.$IP2\.$IP3\.$IP4:$TARGET_PORT/g" {} \;
